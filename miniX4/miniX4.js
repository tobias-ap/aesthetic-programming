let cameraTracker;
let capture;

function videoCapture() {
    //web cam capture
    capture = createCapture(VIDEO);
    capture.size(640, 480);
    capture.hide();
}

function faceTracker() {
    //setup face tracker
    cameraTracker = new clm.tracker();
    cameraTracker.init(pModel);
    cameraTracker.start(capture.elt);
}

function setup() {
    createCanvas(640, 480);
    videoCapture()
    faceTracker()
}


function draw() {

    //draw the captured video on the screen
    image(capture, 0, 0, 640, 480);

    let positions = cameraTracker.getCurrentPosition();
    //check the availability of web cam tracking
    if (positions.length) {

        // RED FACE:

        // Drawing the face and positioning it to the left of me
        beginShape();
  
        for (let i = 1; i <= 19; i++) {
            stroke(0)
            fill(178, 0, 0, 150)
            let faceCover = positions[i]
            vertex(faceCover[0] - 150, faceCover[1] - 150);
        }
        endShape(CLOSE);

        // Drawing the eyes and pupils looking down at me
        for (let i of [27]) {
            fill(255);
            circle(positions[i][0] - 150, positions[i][1] - 150, 20);
            fill(0)
            circle(positions[i][0] - 146, positions[i][1] - 146, 10);
        }
        for (let i of [32]) {
            fill(255)
            circle(positions[i][0] - 150, positions[i][1] - 150, 20);
            fill(0)
            circle(positions[i][0] - 146, positions[i][1] - 146, 10);
        }
        // Drawing the duct tape that covers the mouth
        for (let i of [44]) {
            fill(150)
            rect(positions[i][0] - 160, positions[i][1] - 165, 70, 40);
        }



        // BLUE FACE:

        // drawing another face on the other side of me in the color blue
        beginShape();
        for (let i = 1; i <= 19; i++) {
            fill(100, 200, 255, 150)
            let faceCover = positions[i]
            vertex(faceCover[0] + 150, faceCover[1] - 150);

        }
        endShape(CLOSE);

        // Drawing two eyes again looking down at me
        for (let i of [27]) {
            fill(255);
            circle(positions[i][0] + 150, positions[i][1] - 150, 25);
            fill(0)
            circle(positions[i][0] + 146, positions[i][1] - 146, 10);
        }
        for (let i of [32]) {
            fill(255)
            circle(positions[i][0] + 150, positions[i][1] - 150, 25);
            fill(0)
            circle(positions[i][0] + 146, positions[i][1] - 146, 10);
        }

        // Drawing the suprised mouth
        for (let i of [44]) {
            stroke(200, 0, 0)
            strokeWeight(4);
            fill(40)
            circle(positions[i][0] + 170, positions[i][1] - 155, 40);
        }
    }
    // using the posterize filter with a random value between 2 and 40 to create a distorted look.
filter(POSTERIZE,random(2,40))

}