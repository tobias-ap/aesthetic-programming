# MiniX4 "Waching my every move"
<br>


Look up, they’re always watching what I do, they’re always watching my every move. But who are “they”. In today’s society, “they” have become a buzzword, spoken so often, that we don’t understand who “they” are. In my work “Watching my every move”, “they” are shown as two figures, always watching me, looking down at me, judging me. As the red figure has their mouth taped over, they seem to have captured sensitive information, that they can’t view publicly, but the data has already been captured. The blue figure looks surprised and has captured data that didn’t match the profile that was created around me by “them”. This new information is building the profile further and making it more and more personalized.
<br>
This work shows how we’re always being watched in our everyday life in a playful, but serious way.
<br>

![screenshot of the code here](https://tobias-ap.gitlab.io/aesthetic-programming/miniX4/minix4.png)
<br>

Please run the code [here](https://tobias-ap.gitlab.io/aesthetic-programming/miniX4/miniX4.html)

<br>
Please view the full repository [here](https://gitlab.com/tobias-ap/aesthetic-programming.git)
<br>
In my program, I’ve used the webcam in my laptop as my capturing device. As we are being watched on a daily basis, I thought that this was the most relevant data capture to use. By using the createCapture(VIDEO), I could use the webcam, when permission is given. Other than using the webcam, I used a facetracker. By using the additional p5 libraries, my program was able to track different points across my face. I used these points to create the two figures following my face and making they eyes appear where mine are on my face. But to make them appear beside me and not on my face, I shifted their values to either side. To create the shape, I used the beginShape() function and endShape() function and made both “head” shapes.
<br>
My program clearly shows how we’re being watched. The two figures are above me and are somewhat translucent but clearly defined as well. I made this decision, because we always know that there is data being captured when using our phones, or anything electronic really, but don’t seem to care. The figures are passively watching me, but never interrupting or disturbing me, as that would make me much more aware of their presence.
<br>

<br>
refferences:
<br>
https://p5js.org/reference/

