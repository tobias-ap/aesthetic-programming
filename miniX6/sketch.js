let circleXValue = 0
let circleYValue = 0
let speed = 10
let gravity = 0.2
let ball;
let conditionMet = false;


function setup() {
    createCanvas(windowWidth, windowHeight);
    resetBall ();
      // Check the condition every 100 milliseconds
  let interval = setInterval(checkCondition, 5000);
  
  // Stop checking after 5 seconds
  setTimeout(() => {
    clearInterval(interval);
  }, 20000);
}
function checkCondition() {
    // Replace this with your actual condition
    // For example, checking if mouseX is greater than 200
    if (circleYValue > windowHeight-150) {
      conditionMet = true;
    }
  }

function draw() {
    background(0)
frameRate(120)


    target = new Target(windowWidth * noise(0.01 * frameCount))
    ball = new Ball(circleXValue,circleYValue)
    mapdetails = new MapDetails()

    mapdetails.display()

    target.display()
   
    ball.display()
    ball.move()

    collisionDetection()
    if (conditionMet) {
        mapdetails.displayGameOver()
    } 

}
function collisionDetection(){
if (ball.circleY + ball.circleDiameter/2 >= target.rectY &&
    ball.circleY <= target.rectY + target.rectHeight + 10 &&
    ball.circleX + ball.circleDiameter >= target.rectX &&
    ball.circleX <= target.rectX + target.rectWidth
) {
resetBall();
}
}
function resetBall (){
    circleYValue = 0;
}