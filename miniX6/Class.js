class Target {
    constructor(x) {
        this.rectXStart = x
        this.rectYStart = windowHeight - this.rectHeight
        this.rectHeight = 10;
        this.rectX = x
        this.rectY = windowHeight - this.rectHeight
        this.rectWidth = 80;
        this.rectMin = 0
        this.rectMax = windowWidth - this.rectWidth
    }
    display() {
        noStroke()
        fill(0, 255, 0)
        rect(this.rectX, this.rectY, this.rectWidth, this.rectHeight)
    }
}

class Ball {
    constructor(x, y) {
        this.circleX = x
        this.circleY = y
        this.circleDiameter = 20
        this.circleMin = 0
        this.circleMax = windowWidth - this.circleDiameter
    }

    move() {
        circleYValue = circleYValue + speed;
        speed = speed + gravity;

        if (keyCode === LEFT_ARROW) {
            circleXValue -= 15;
        } else if (keyCode === RIGHT_ARROW) {
            circleXValue += 15;
        }

        if (circleYValue > windowHeight-this.circleDiameter) {
            //reverse the speed
            speed = -0.80 * speed;
        }
        if (this.circleX < 0 + this.circleDiameter) {
            circleXValue += 15
        }
        if (this.circleX > windowWidth - this.circleDiameter) {
            circleXValue -= 15
        }

    }
    display() {
        fill(255)
        circle(this.circleX, this.circleY, this.circleDiameter)
    }
}

class MapDetails{
    display(){
        stroke(255,0,0,155)
        line(0,windowHeight-150,windowWidth,windowHeight-150)
        fill(0,255,0)
        textSize(50)
        textAlign(CENTER)
        text('GNOP',windowWidth/2,50)
        textSize(20)

        text('press arrow keys to move the ball and hit the moving target',windowWidth/2,70)
        fill(255,0,0)
        text('dont stay under the red line for too long',windowWidth/2,90)


    }
    displayGameOver(){
                textSize(200)
                textAlign(CENTER)
        text('GAME',windowWidth/2,windowHeight/2)
        text('OVER',windowWidth/2,windowHeight/1.4)
    }

}