# MiniX6 "GNOP"
<br>
My game is made up of two primary objects. The ball and the target. A third object, the map details, are less important, but add to the features and the aesthetic.
<br>
The ball object is made up of a circle and different conditional statements, that control its movement. Interaction with the arrow keys and "gravity-like" movement everytime the ball hits the buttom of the screen.
<br>
The target object is made up of a rectangle located at the buttom of the screen. This target moves randomly across the x-axis by using the noise() function.
<br>
When the ball collides with the target, by controlling the ball with the arrow keys, the ball resets at the top of the screen and drops again. However, if the ball is below the red line for 5 seconds or more, without hitting the target, the mapDetails object will display GAME OVER.
<br>

![screenshot of the code here](https://tobias-ap.gitlab.io/aesthetic-programming/miniX6/GNOP.png)
<br>

Please run the code [here](https://tobias-ap.gitlab.io/aesthetic-programming/miniX6/miniX6.html)

<br>
Please view the full repository [here](https://gitlab.com/tobias-ap/aesthetic-programming.git)
<br>
Taking reference from the collision of a classic pong game program, i wanted to make something reversed. Controlling the ball instead og what i call the target. The main method in my game, is the movement of the ball. Using a method that makes the ball move as if gravity was controlling it to create a sense of urgency and connecting the game to real life in that way. 
<br>
Object oriented programming is connecting human thinking to computational thinking. Linking these better than much else, has made human computer interaction so much better through the years. While abstraction draws from the real world, in a way of onlyshowing whats necessary to understand the program. Making code readeable not only for machines, but for humans as well.
<br>
refferences:
<br>
https://p5js.org/reference/
<br>
https://editor.p5js.org/ellacyt/sketches/B1lmPZgoZ 
<br>
https://www.youtube.com/watch?v=IIrC5Qcb2G4 
