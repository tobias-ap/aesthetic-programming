function setup() {
    createCanvas(windowWidth, windowHeight);
    frameRate(120)
    background(255, 200, 150)

}

function draw() {
// making a rain-like effect appear every 30 seconds
if (second()>30) {
// using a for-loop to create a bunch of lines beside eachother to appear as rain
        for (let x = 0; x < windowWidth; x++) {
    stroke(150,200,255,6)
line(x,0,x,random(0,windowHeight-random(0,windowHeight-200)))
}
}
// defining variables to generate the circles
    let xCircles = random(0, windowWidth);
    let yCircles = random(0, windowHeight);
    let dCircles = random(1, 60);
// using if-statements to make the circles appear in different colors in different places
    if (xCircles < windowWidth / 1.5) {
        fill(3, random(120, 255), 180)
        noStroke()

    } else {
        fill(0, 0, random(100, 200))
        stroke(255, 240, 200)
        strokeWeight(5)
    }
    if (yCircles < windowHeight / 1.5) {
        noStroke()
    } else {
        stroke(255, 240, 200)
    }
    circle(xCircles, yCircles, dCircles)
}
