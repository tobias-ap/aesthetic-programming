# MiniX2 upside down

In this week’s miniX, we had to program two emojis. For this assignment, I wanted to make one emoji and then the same emoji reflected in different colors. These two objects would never appear at the same time, as they’re programmed into an if-else-statement: When the x value of the mouse is <200, the first emoji appears, and when the value is >200, the second “reflected” emoji appears. These two emojis are programmed to look sort of confused, lost, dissatisfied. To get this look, I made the eyes uneven and used a Bezier command for the mouth. The Bezier was hard to understand, as there are eight values to adjust. But with some trial and error, I got to the desired look.
![screenshot of the code here](https://tobias-ap.gitlab.io/aesthetic-programming/miniX2/Skærmbillede_2024-03-10_kl._15.27.36.png)
<br>

Please run the code [here](https://tobias-ap.gitlab.io/aesthetic-programming/miniX2/minix2.html)

<br>
Please view the full repository [here](https://gitlab.com/tobias-ap/aesthetic-programming.git)


These two emojis are not of a traditional look. The one that’s right side up uses a completely white base color as if the intended color of the emoji hadn’t loaded. On the other hand, the upside down one uses a more traditional color for an emoji. Being upside down puts it in a different perspective, as it shows that emoji-culture may be somewhat at fault by how inclusive or non-inclusive it should be. From what started as simple ways to show your emotions in text form as “:)” or “;)”, has turned to be a subject of conversation. 
My emojis don’t look too happy in general. Maybe they aren’t happy about how the culture around emojis has evolved into something more than just a form of expression. And their eyes seem to look blind or somewhat see-through. As these two emojis reflect each other, they never share the same space. Their confused emotions never seem resolve as the culture around them keeps on wanting more from them.

refferences:
https://p5js.org/reference/

