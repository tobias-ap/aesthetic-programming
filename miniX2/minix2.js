function setup() {
  createCanvas(400, 500);
  angleMode(DEGREES);
  background(110, 0, 0)


}

function draw() {
  // general design decissions 
  stroke(0, 0, 100)
  strokeWeight(20);
  fill(255, 235, 200)
  rect(-50, 250, 400, 260)
  ellipse(240, 30, 390, 390)
  strokeWeight(5);

  // if-statement that shows one emoji when x<200 and another when x>200
  if (mouseX < 200) {
    // first emoji
    ellipseMode(CENTER);
    fill(255);
    ellipse(200, 150, 150, 150);
    fill(255, 200, 0, 100)
    ellipse(170, 150, 50, 40)
    ellipse(230, 145, 40, 50)

    // mouth
    noFill()
    bezier(230, 200, 200, 200, 190, 190, 190, 200);
  }
  else {
    // second emoji
    fill(255, 200, 0)
    ellipse(200, 350, 150, 150);
    fill(255)
    ellipse(170, 355, 50, 40)
    ellipse(230, 360, 40, 50)

    // mouth
    noFill()
    bezier(230, 320, 200, 320, 190, 310, 190, 320);
  }

}