// Angle of rotation: the parameter that we'll animate!
let angle = 0;

function setup() {
  createCanvas(windowWidth, windowHeight);
  angleMode(DEGREES);
}

function draw() {
  background(50,100,100);

  // We'll be drawing a square in the center of our sketch that rotates around itself
  push();
  translate(width/2, height/2);


  // The angle of rotation is what we're animating (ie changing over time) so first we'll increase its value and then rotate to that angle
  angle += 0.5;
  rotate(angle);

  // Try changing the amount we add to the angle and see how the animation changes! (Hint: we can think of this as 'speed')

  // Then draw the square
  fill(125,250,125);
  noStroke();
  rectMode(CENTER);
  ellipse(0,0, 450,30);
  pop();

  // A slightly fancier addition, but really this just builds on what we did above! We use a for-loop to create a ring of squares 30º degrees apart around the center
  for (let a=0; a<360; a+=30) {
    fill(254,254,200)
    push();
    translate(width/2, height/2);   // move origin to center
    rotate(a);                      // rotate each by 30º
    translate(0, 200);              // then offset vertically
    rotate(-angle);                 // spin around the other way!
    rectMode(CENTER);
    rect(0,0,10,50,10);
    pop();
  }
  textSize(200);
  fill('yellow');
  text('"time"', 20, 100);
  fill('cornflowerblue');
  text('"time"', 35, 140);
  fill('tomato');
  text('"time"', 10, 80);
  fill('lightgreen');
  text('"TIME"', 15, 140);
}



