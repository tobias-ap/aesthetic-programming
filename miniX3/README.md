# MiniX3 "Time is a social construct"
<br>
This week for our miniX we had to experiment with throbbers. Generally throbbers are annoying when our computers show them. I mostly have experience with them when the internet is slow or when a program is overloading the cpu of my computer. The classic rainbow colored throbber on a mac is certain to make your brain eclipse.
In my throbber, i wanted to illustrate the passing of time. Starting by making a clock and then experimenting with the values of the different parameters to make the clock super distorted. When a throbber never seems to disappear, time is just passing and you'll never get that time back. I wanted to make that very obvious in my code. The numbers are turning with the clock hands and they're turning way to fast, to illustrate the pasing of time. The circle framing the entire clock is also vibrating,  as if its trying to escape the screen. 
![screenshot of the code here](https://tobias-ap.gitlab.io/aesthetic-programming/miniX3/minix3.png)
<br>

Please run the code [here](https://tobias-ap.gitlab.io/aesthetic-programming/miniX3/minix3.html)

<br>
Please view the full repository [here](https://gitlab.com/tobias-ap/aesthetic-programming.git)
<br>
In my code, I've used rotate() and random() as the primary functions to make my throbber have movement. rotate() makes the clockhands and numbers go around and while using different values for the different clock hands, they move in different tempoes while stille using the same function. This ensures that i don't repeat myself to much. 
<br>
For many of the parameters I used random() to make the sizes and colors change randomly to further distort the overall look of the throbber. Making it look very busy and on the verge of breaking. 
<br>
To make trails of the clock hands and the numbers, i made sure to use a low alpha value on the background color. As the code refreshes 60 times per second, the added layers of background color make the hands and numbers disappear slower than they otherwise would.
<br>

refferences:
<br>
https://p5js.org/reference/

[Tutorial for making a clock](https://www.youtube.com/watch?v%253D75r1Qu71REo)

