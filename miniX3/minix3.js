function setup() {
  createCanvas(1000, 1000);
  frameRate(120)
}

function draw() {
  // The orange background color with a low alpha value to make "trails" from the numbers and clockhands
  background(200, 100, 50, 30);
  // Making everything centered
  translate(width / 2, height / 2)

  // the yellow color of the clockhands
  stroke(255, 200, 0)

  // using push() pop() to make only the numbers and hands rotate
  push();
  // using the hands() function with different values.
  hands(millis() / 200, 230, 4);
  hands(millis() / 300, 200, 8);
  hands(millis() / 400, 150, 12);
 
  // Calling the numbers() function
  numbers()
  pop();

  // making the large circle, framing everything. Making the strokeweight random to make it move
  strokeWeight(random(1, 20));
  stroke(255, 230, 100)
  noFill()
  circle(0, 0, 500)

  // making the small white triangle i the center of everything
  fill(255)
  noStroke()
  triangle(-12, 8, 10, 12, 2, -10);




  // using filter to apply blur to the image, making everything less pronounced
  filter(BLUR, 3)


}

// creating the function hands() to make the clockhands with different parameters to make different rotation speeds, lengths, and strokeweights
function hands(rotation, length, weight) {
  strokeWeight(weight)
  rotate(rotation)
  line(0, 0, 0, -length);
}

// creating the numbers function using a for loop and using the the numbers 1-12 and formatting the text with some random parameters
function numbers() {
  for (let i = 1; i <= 12; i++) {
    noStroke();
    fill(random(80, 150), 0, 0);
    textSize(random(40, 100));
    textFont('Times');
    textStyle(BOLD);

  // placing the text in a circle and experimenting with the values to place them randomly
    text(i,
      map(cos(i * 10), -1, 1, -170, 100),
      map(sin(i * 42), -1, 1, -100, 170)
    );
  }
}
