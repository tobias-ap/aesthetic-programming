function setup() {
  createCanvas(4000, 5000);
  background(0, 50, 100)
  noStroke()


}

function draw() {
  // røde farver
  fill(230, 10, 3, 200)
  rect(0, 0, 4000, 100)
  // rect(350, 0, 90, 500)
  circle(1800, 800, 900)
  rect(2210, 800, 40, 3000)
  rect(300, 4000, 400, 1000)

  // beige farver
  fill(240, 240, 200)
  rect(3000, 0, 900, 5000)

 // gule farver
  fill(255, 200, 0)
  rect(2210, 1100, 40, 3000)
  circle(1800, 1100, 900)

  // lyseblå farver
  fill(40, 80, 200)
  circle(4000, 5000, 5000)
  rect(500, 4000, 400, 1000)

    // røde farver
    fill(230, 10, 3, 200)
    circle(3800, 5000, 4500)

    // gule farver
  fill(255, 200, 0)
  circle(3600, 5000, 4000)

  // mørkeblå farver
  fill(0, 50, 100)
  circle(3400, 5000, 3500)


  // beige farver
  fill(240, 240, 200)
  rect(0,4500,4000,500)
  

  // text
  textSize(980);
  fill(240, 240, 200,100);
  text('miniX1', 210, 1000);
  textSize(150);
  fill(0, 50, 100);
  text('AE.DD.2024', 3100, 4800);


}


// mindre størrelse
// function setup() {
//   createCanvas(400, 500);
//   angleMode(DEGREES);
//   background(0, 50, 100)
//   noStroke()


// }

// function draw() {
//   // røde farver
//   fill(230, 10, 3, 200)
//   rect(0, 0, 400, 10)
//   // rect(350, 0, 90, 500)
//   circle(180, 80, 90)
//   rect(221, 80, 4, 300)
//   rect(30, 400, 40, 100)




//   // beige farver
//   fill(240, 240, 200)
//   rect(300, 0, 90, 500)

//  // gule farver
//   fill(255, 200, 0)
//   rect(221, 110, 4, 300)
//   circle(180, 110, 90)

//   // lyseblå farver
//   fill(40, 80, 200)
//   circle(400, 500, 500)
//   rect(50, 400, 40, 100)

//     // røde farver
//     fill(230, 10, 3, 200)
//     circle(380, 500, 450)

//     // gule farver
//   fill(255, 200, 0)
//   circle(360, 500, 400)

//   // mørkeblå farver
//   fill(0, 50, 100)
//   circle(340, 500, 350)


//   // beige farver
//   fill(240, 240, 200)
//   rect(0,450,400,50)
  

//   // text
//   textSize(98);
//   fill(240, 240, 200,100);
//   text('miniX1', 20, 100);
//   textSize(15);
//   fill(0, 50, 100);
//   text('AE.DD.2024', 310, 480);


// }

