# MiniX1 Poster

After six intense fundamental lectures with lots of information and syntax to remember, p5.js was very refreshing. It seems much simpler and easier, compared to the other program languages we learned. Making something aesthetic especially. Because of this, I wanted to play with making some sort of poster for my first miniX. I have never really tried to make a poster before, so at first it seemed intriguing. I found a background color I liked by playing with RGB values and then tried different shapes on top. I played around a lot and ended up with a design I enjoy. The process was a bit tedious compared to using something like photoshop, where you can drag your objects wherever you want instead of typing the coordinates. These limitations also made me reflect more on my choices because of the tiresome process of trying lots of different things. Overall, I really enjoyed the process. It was very unique and made programming less frustrating.

![picture of the poster](https://tobias-ap.gitlab.io/aesthetic-programming/miniX1/MiniX1_stor.png )
<br>

Please run the code [here](https://tobias-ap.gitlab.io/aesthetic-programming/miniX1/index-kopi.html)

<!--- Linket skal formatteres sådan som jeg har gjort det - https://{GRUPPE NAVN}.gitlab.io/{PROJEKT NAVN}{miniX DER SKAL SES} -->
<br>
Please view the full repository [here](https://gitlab.com/tobias-ap/aesthetic-programming.git)
<!--- Det her link kan I bare kopiere fra jeres browser, kopier linked når I befinder jer I mappen -->


<br>
When coding my miniX, I experimented with shapes, RGB color and text to make my design. It was easy to find the syntax I needed in the reference list on https://p5js.org/reference/. 
Coding is very new to me and is something I’ve felt very intrigued by. I don’t think there’re a lot of other activities that are similar to it. Reading and writing text seems very different from reading and writing code. It truly is a language that we have to learn to make the experience more fluid, like when writing and reading text. I've never seen coding as something creative. It usually has a right and a wrong way of doing thing and the code has to work. But trying to code creatively has enlightened me. This first miniX is making me look farward to the next ones and having concrete challenges will motivate me.


### **References**
Syntax from:
<br>
https://p5js.org/reference/ 