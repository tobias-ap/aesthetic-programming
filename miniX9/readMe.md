# MiniX9 "Flowchart"
<br>
When trying to communicate a program as simply as possible via a flowchart, you start to question your own code. How complex does it need to be, as the simple version of it (the flowchart) seems "easy" to comprehend. But the process of simplifying your own code seems like a good idea. This process makes sure that i understand everything i wrote. I think it would be a good idea to do flowcharts for every single miniX before the exam to get a good grasp of them all. 
<br>
When making the flowchart, choosing the right shapes of boxes and the directions of the arrows, made me realise how my program worked on a surface level, rather than on a "code level".
<br>

![screenshot of the flowchart here](https://tobias-ap.gitlab.io/aesthetic-programming/miniX9/miniX9.png)
<br>

![screenshot of the group flowchart here](https://tobias-ap.gitlab.io/aesthetic-programming/miniX9/groupFlowchart.jpg)
<br>

<br>
Please view the full repository [here](https://gitlab.com/tobias-ap/aesthetic-programming.git)
<br>
Our two ideas are very different. One is a game and the other is not. When generating these ideas, we disussed the technical side throughout. But as the ideas were so different from eachother, this made choosing difficult. We ended up choosing the game, as it seemed to be more interesting codewise. 
<br>
When we started to program the outline of our project, it was a bit harder than we thought. Making the counter increase incrementally and decreasing when making a purchase, was the first step. This ended up working, and now we are on to the next challenge of making the layout.
<br>
Both of the flowcharts are useful, as they show the process. My own made me see my code differently, and the group flowchart gives us something to follow when making our program. This helps us to keep us on track and not stray too far from the original idea.
<br>
refferences:
<br>
https://p5js.org/reference/

